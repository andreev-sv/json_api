class PostsController < ApplicationController
  def index
    @posts = Posts.all

  end
  def create
    @post = Post.create(post_params)
        if @post
          render json: @post, status: 200
        else
          render json: @post.errors, status: 422
    end
  end

  private

  def post_params
    params.require(:post).permit(:title, :body, :ip, :user_login)
  end

end
